<?php
/*
YARPP Template: Giup ban lam dep
Author: Quoc Nguyen
Description: Bai viet lien quan cua GBLD - widget
*/
?><h4 class="widget-title">CÁC BÀI VIẾT LIÊN QUAN</h4>
<?php  if (have_posts()):
    $i = 1; ?>
    <ol>
        <li><a href="<?php echo get_permalink( '7062' ); ?>" target="_blank" title="<?php echo get_the_title('7062'); ?>"><?php echo get_the_post_thumbnail( 'post-thumbnail',  array( 'class' => 'alignleft' ) );?> <?php echo get_the_title('7062'); ?></a></li>
        <li><a href="<?php echo get_permalink( '3788' ); ?>" target="_blank" title="<?php echo get_the_title('3788'); ?>"><?php echo get_the_post_thumbnail( 'post-thumbnail',  array( 'class' => 'alignleft' ) );?> <?php echo get_the_title('3788'); ?></a></li>
        <li><a href="<?php echo get_permalink( '4881' ); ?>" target="_blank" title="<?php echo get_the_title('4881'); ?>"><?php echo get_the_post_thumbnail( 'post-thumbnail',  array( 'class' => 'alignleft' ) );?> <?php echo get_the_title('4881'); ?></a></li>
        <?php while (have_posts()) : the_post();  if ($i <= 2) {  ?>
            <?php if (has_post_thumbnail()):?>
                <li><a target="_blank" href="<?php the_permalink() ?>" rel="bookmark"> <?php the_post_thumbnail('post-thumbnail', array( 'class' => 'alignleft' )); ?><?php the_title(); ?></a><!-- (<?php the_score(); ?>)--></li>
            <?php endif; ?>
            <?php
            $i++;
        } endwhile;   ?>
    </ol>
<?php else: ?>
    <p>KHÔNG CÓ BÀI VIẾT LIÊN QUAN.</p>
<?php endif; ?>