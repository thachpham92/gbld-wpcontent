			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<div id="inner-footer" class="wrap cf">

					<nav role="navigation">
						<?php wp_nav_menu(array(
    					'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
    					'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
    					'menu' => __( 'Footer Links', 'bonestheme' ),   // nav name
    					'menu_class' => 'nav footer-nav cf',            // adding custom nav class
    					'theme_location' => 'footer-links',             // where it's located in the theme
    					'before' => '',                                 // before the menu
    					'after' => '',                                  // after the menu
    					'link_before' => '',                            // before each link
    					'link_after' => '',                             // after each link
    					'depth' => 0,                                   // limit the depth of the nav
    					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
						)); ?>
					</nav>

					<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.</p>
					<p class="source-org copyright" style="font-size: .8em">Giupbanlamdep.com là blog cá nhân của Oanh Phạm để chia sẻ về các kiến thức làm đẹp cho phụ nữ như cách làm trắng da, trị mụn, đánh giá mỹ phẩm. Xin giữ lại bản quyền &copy;Giupbanlamdep.com nếu sao chép nội dung từ blog này.</p>
				</div>

			</footer>

		</div>
		<?php echo do_shortcode('[supercarousel id=8348]') ?>
		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>
			<div id="fb-root"></div>
			<script>(function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s); js.id = id;
					js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=512214205517376";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
	</body>

</html> <!-- end of site. what a ride! -->
