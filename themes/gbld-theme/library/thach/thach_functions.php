<?php
/* Tối ưu SEO logo
------------------------ */
function tp_seo_logo() {
	
	$tag = 'h1';
	if ( !is_home() ) {
		$tag = 'p';
	}
	return $tag;

}


/* Shortcode hiển thị bài viết gợi ý */
function goiy_create_shortcode($att) {
	$goiy = new WP_Query(array(
			'p' => $att['id'],
			'post_type' => 'goi_y'
	));

	ob_start();
	echo '<div class="related-posts">';
		if ($goiy->have_posts()) : while ($goiy->have_posts()) : $goiy->the_post();
			$goiy_link = get_post_meta( $goiy->post->ID, 'goiy_url', true );
			echo '<div class="post">';
			echo '<div class="left fill">';
			echo '<a href="'.$goiy_link.'" class="entry-title">';
				the_post_thumbnail('thumb-small');
			echo '</a>';
			echo '</div>';
			echo '<div class="right">';
			echo '<a href="'.$goiy_link.'" class="entry-title">'.get_the_title().'</a>';
				echo '<div class="entry-content">'.get_the_excerpt().'</div>';
			echo '</div>';
		echo '</div>';

	endwhile; endif;
	echo '</div>';

	$list_post = ob_get_contents(); //Lấy toàn bộ nội dung phía trên bỏ vào biến $list_post để return

	ob_end_clean();

	return $list_post;
}
add_shortcode('goiy', 'goiy_create_shortcode');






?>